.. _models:

Models
======

The ``dpwagtail`` application contains models for managing the existence and state of Dash
apps, along with some Wagyail page models.


Django models
-------------

The ``dpwagtail.models`` module contains Django models for managing the existence and state
of Dash apps.

InstanceBuilder
    Model controlling the building of ``DashInstance`` objects. Essentially, an ``InstanceBuilder`` generates
    new instances of Dash apps, with shared initial parameters.

    During setup, or at any time using the admin interface or as part of the ``add_initial_items`` management
    command, default instances of this model are added to the database.

DashInstance
    Model specifying the formation of Dash applications. This can be done as stateless, or per user, and with each
    ``DashInstance`` providing different initial values for each instance.


Wagtail page models
-------------------

DjangoPlotlyDashIndexPage
    Wagtail page model, that explicitly collects live
    child pages of type ``DjangoPlotlyDashPage``
    
DjangoPlotlyDashPage
    Page containing a single ``DashInstance`` along with long and short descriptions.
