.. Django Plotly Wagtail documentation master file, created by
   sphinx-quickstart on Wed May  3 13:50:18 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Django Plotly Wagtail's documentation!
=================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   introduction
   installation
   models
   demo
   development


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
