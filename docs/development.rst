.. _development:

Development
===========

Start by cloning the git repository and then using the scripts to setup the environment. All
of the scripts live within the **scripts** subdirectory.

The ``scripts/make_env.bash`` script, run from the root
directory of the code, will set up a virtual environment and install the needed
packages for both use and development of the project, as well as ones used
by the demo application. After this, the ``scripts/start.bash`` script
can be sourced from the root directory to set up necessary environment variables and activate
the virtualenv.

Note that all the scripts are aimed for use with Linux. They may work on other environments, such
as those available as GitBash and Cygwin on Windows, but this is not guaranteed or supported.


Scripts
-------

See the **scripts** directory for a number of useful scripts when
developing django-plotly-wagtail.

+------------------------+-------------------------------------------------------+
| Name                   | Script                                                |
+========================+=======================================================+
| setup.bash             | Load environment variables and form the environment   |
+------------------------+-------------------------------------------------------+
| build_packages.bash    | Build PyPi packages and form the sphinx documentation |
+------------------------+-------------------------------------------------------+
| run_tests.bash         | Run all of the tests                                  |
+------------------------+-------------------------------------------------------+
| setup_demo_server.bash | Set up the demonstration server                       |
+------------------------+-------------------------------------------------------+
| run_demo_server.bash   | Launch the demo server                                |
+------------------------+-------------------------------------------------------+
| run_tests.bash         | Run all of the tests                                  |
+------------------------+-------------------------------------------------------+

