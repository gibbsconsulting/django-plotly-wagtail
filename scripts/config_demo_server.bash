#!/usr/bin/env bash
#
# Build and launch the demo server
#
# In case anything is added to the startup for the environment
source scripts/start.bash

# Ensure staticifles directory exists
mkdir -p demo/demo/static

# Add superuser and config
./scripts/run_demo_server.bash shell < scripts/config.py
./scripts/run_demo_server.bash collectstatic -i "*.py" -i "*.py" --noinput --link

# Initial migrate and setup command
./scripts/run_demo_server.bash add_initial_items
./scripts/run_demo_server.bash add_app_pages

# Now launch the server
./scripts/run_demo_server.bash runserver
