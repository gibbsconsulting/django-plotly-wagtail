#!/usr/bin/env bash
#
source scripts/env_vars.bash
#
mkdir -p env
rm -rf env
python$1 -m venv env
source env/bin/activate
pip install -U pip
pip install -r scripts/requirements.txt
pip install -r scripts/dev_requirements.txt
pip install -r scripts/demo_requirements.txt
# Add local source only once the infrastructure has been updated
pip install -e src
