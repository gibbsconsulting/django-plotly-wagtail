#!/usr/bin/env bash
#
source scripts/start.bash
#
pushd src
python setup.py sdist
python setup.py bdist_wheel
popd
pushd docs
make html
popd
