# Generated by Django 4.2.6 on 2023-12-02 04:32

from django.db import migrations
import wagtail.fields


class Migration(migrations.Migration):

    dependencies = [
        ('dpwagtail', '0002_djangoplotlydashindexpage'),
    ]

    operations = [
        migrations.AddField(
            model_name='djangoplotlydashpage',
            name='short_description',
            field=wagtail.fields.RichTextField(blank=True),
        ),
    ]
