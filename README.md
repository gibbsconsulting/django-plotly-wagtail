# django-plotly-wagtail

This package links together django-plotly-dash and wagtail.


## Installation

To install, just use pip:


    pip install django-plotly-wagtail


See the prequisites section for a description of the necessary environment and the
getting started section for a description of how to begin using this package.


## Prerequisites

The prerequisites are
- django (versions > 3.2)
- django-plotly-dash
- wagtail

Python versions >= 3.9 are supported. The project is only tested on Linux and might work on other OS.


## Getting started

Once installed, add django-plotly-wagtail to your project's settings.py file.

More detailed information
can be found in the online documentation at
<https://readthedocs.org/projects/django-plotly-wagtail>


## Demo

The source repository contains a rudimentary demonstration Django application.
The quickstart for the demo is to first clone the gitlab respository, and then from the
root of the clone use


    ./scripts/setup_demo_server.bash


to set up the server environment, and then launch the server with


    ./scripts/run_demo_server.bash


This will launch the demo application using Django's internal development server.

More detailed information
can be found in the online documentation at
<https://readthedocs.org/projects/django-plotly-wagtail>

## Contributing

The parent repository for this project is
<https://gitlab.com/GibbsConsulting/django-plotly-wagtail>

Contributions are welcomed from contributors that have completed a contributor agreement. If you wish to make
a contribution then please send an email to <agreements@gibbsconsulting.ca>


## License

This project is licenced under the GNU AGPL v3. Commercial licencing is also available; send an
email to <licencing@gibbsconsulting.ca> for more information.

