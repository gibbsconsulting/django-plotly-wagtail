# views to facilitate testing


from django.shortcuts import render
from django_plotly_dash.models import find_stateless_by_name, StatelessApp
from dpwagtail.models import DashInstance


def instance_template_tag_render(request, template_name, **kwargs):

    dpd_instance_name = "SimpleExample"
    dash_app = find_stateless_by_name(dpd_instance_name)
    stateless_instance = StatelessApp.objects.get(app_name=dpd_instance_name)
    dpd_instance = DashInstance(stateless_app=stateless_instance)

    return render(request,
                  template_name,
                  locals())


def predefined_render(request, template_name, **kwargs):
    """Render a predefined DashInstance"""
    dpd_instance = DashInstance.objects.get(name="DashInstance")

    return render(request,
                  template_name,
                  locals())


def predefined_render_2(request, template_name, **kwargs):
    """Render a predefined DashInstance take 2"""
    dpd_instance = DashInstance.objects.get(name="Trial Trivial Per-User")

    return render(request,
                  template_name,
                  locals())


def predefined_render_3(request, template_name, **kwargs):
    """Render a predefined DashInstance take 2"""
    dpd_instance = DashInstance.objects.get(name="Trial Trivial IB-arg")

    return render(request,
                  template_name,
                  locals())
