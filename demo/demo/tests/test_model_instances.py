import pytest
import json

from django.urls import reverse
from django_plotly_dash.models import StatelessApp

from dpwagtail.models import InstanceBuilder, DashInstance
from dpwagtail.utils import form_instance_builders


@pytest.mark.django_db
def test_instance_formation():
    ib = InstanceBuilder(name="Erty")
    assert ib is not None
    assert str(ib) == 'Erty'


@pytest.mark.django_db
def test_instance_formation_util():
    ibs = form_instance_builders(InstanceBuilder)
    assert ibs is not None

    ibs = form_instance_builders(InstanceBuilder)
    assert ibs is not None
    for name, new_cnt in ibs:
        assert new_cnt == 0


@pytest.mark.django_db
def test_wagtail_urls(client):
    ib_list = client.get('/dpwagtail/instance_builders/',)
    assert ib_list.status_code == 200


@pytest.mark.django_db
def test_explict_view(client):
    resp = client.get('/tv/1',)
    assert resp.status_code == 200


@pytest.mark.django_db
def test_explict_template(client):
    # Check rendering of a template containing a test dash app
    resp = client.get('/tv/2',)
    assert resp.status_code == 200


@pytest.mark.django_db
def test_dash_callback(client):
    base_request = {"output": "rad_out.children",
                    "outputs": {"id": "rad_out",
                                "property": "children"},
                    "inputs": [{"id": "rad_choice",
                                "property": "value",
                                "value": 0}],
                    "changedPropIds": ["rad_choice.value"]}

    packed = json.dumps(base_request)

    resp = client.post('/dpdash/app/SimpleExample/_dash-update-component',
                       data=packed,
                       content_type='application/json',
                       headers={'Content-Type': 'application/json',
                                'Accept': 'application/json'})

    assert resp.status_code == 200


@pytest.mark.django_db
def test_dash_instance(client):

    sapp = StatelessApp(app_name="SimpleExample")
    ib = InstanceBuilder(name="dash-instance-ib")
    di = DashInstance(name="DashInstance",
                      instance_builder=ib,
                      stateless_app=sapp)
    assert str(di) == 'DashInstance'

    request = None
    din = di.instance_name(request)
    assert str(din) == din
    assert len(di.instance_name(request)) > 0

    resin = di.resolve_instance(request)
    assert resin[0] == 'name'
    assert resin[1] == sapp

    sapp.save()
    ib.save()
    di.save()
    assert di.pk > 0
    assert di.stateless_app is not None

    ib_list = client.get('/tv/3')
    assert ib_list.status_code == 200
