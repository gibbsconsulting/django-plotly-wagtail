"""Check the formation of dash instances under differnt circumstances"""

import pytest

from django_plotly_dash.models import StatelessApp

from dpwagtail.models import InstanceBuilder, DashInstance
from dpwagtail.utils import form_instance_builders


class MockUserInfo:
    def __init__(self):
        self.username = "Mock Name"


class MockRequest:
    def __init__(self):
        self.user = MockUserInfo()


@pytest.mark.django_db
def test_dash_instance_formation(client):
    """Create a DashInstance and exercise it"""

    form_instance_builders(InstanceBuilder)
    ib = InstanceBuilder.objects.get(name="Trivial no-arguments")
    assert ib is not None

    sa = StatelessApp(app_name="SimpleExample")
    sa.save()

    di = DashInstance(name="Trial Trivial No-arg",
                      instance_builder=ib,
                      stateless_app=sa)
    assert di.name == "Trial Trivial No-arg"

    req = None
    assert di.instance_name(req) == str(sa)

    name, instance = di.resolve_instance(req)
    assert name == "name"
    assert instance == sa
    assert len(di.slug) < 1

    di.save()
    assert di.slug == 'trial-trivial-no-arg'

    di2 = DashInstance(name="Trial Trivial Per-User",
                       instance_mode=DashInstance.IMC_PER_USER,
                       stateless_app=sa,
                       instance_builder=ib)
    di2.save()

    name = None
    try:
        name, instance = di2.resolve_instance(req)
    except:
        pass
    assert name is None

    req = MockRequest()
    assert di2.instance_name(req) == "SimpleExample :user: Mock Name"

    name, instance = di2.resolve_instance(req)
    assert name == 'da'
    assert instance.instance_name == di2.instance_name(req)
    assert instance.slug == 'simpleexample-user-mock-name'

    name2, instance2 = di2.resolve_instance(req)
    
    assert name2 == name
    assert instance == instance2

    ib_list = client.get('/tv/4')
    assert ib_list.status_code == 200


@pytest.mark.django_db
def test_dash_instance_formation_with_state(client):

    base_state = {'__non_app': {'save_on_change': True},
                  'rad_choice_2': {'value': '1'}}

    ib = InstanceBuilder(name="Instance With State",
                         args=base_state)
    ib.save()

    sa = StatelessApp(app_name="SimpleExample")
    sa.save()

    di = DashInstance(name="Trial Trivial IB-arg",
                      instance_mode=DashInstance.IMC_PER_USER,
                      instance_builder=ib,
                      stateless_app=sa)
    di.save()
    assert di.name == "Trial Trivial IB-arg"

    req = MockRequest()
    name, instance = di.resolve_instance(req)

    assert instance.base_state == '{"rad_choice_2": {"value": "1"}}'

    ib_list = client.get('/tv/5')
    assert ib_list.status_code == 200

